FROM nginx:1.20-alpine
WORKDIR /etc/nginx/conf.d
RUN rm -f default.conf
COPY ./revproxy.conf.template ./
CMD ["/bin/sh","-c","envsubst < revproxy.conf.template > revproxy.conf && nginx -g 'daemon off;'"]
